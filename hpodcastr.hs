import qualified Data.ByteString.Lazy as B
import Network.HTTP
import Network.URI (parseURI)
import Data.List
import Data.Maybe
import Text.XML.Light


data Episode = Episode { title, link, description :: String } 
               deriving (Show)

 
main = do
    html <- openUrl "http://downloads.bbc.co.uk/podcasts/worldservice/newshour/rss.xml"
    case (parseXMLDoc html) of
        Just element -> mapM_ downloadEpisode episodes
            where items = findElements (QName "item" Nothing Nothing) element
                  episodes = map getEpisode items
        Nothing -> error "Could not parse feed"


openUrl :: String  -> IO B.ByteString
openUrl url = let uri = case parseURI url of
                      Nothing -> error $ "Invalid URI: " ++ url
                      Just u -> u in
          simpleHTTP (mkRequest GET uri) >>= getResponseBody


getEpisode :: Element -> Episode
getEpisode item = Episode (getVal "title") (getVal "link") (getVal "description")
    where getVal n = maybe "" strContent (findElement (QName n Nothing Nothing) item)


downloadEpisode :: Episode -> IO()
downloadEpisode episode = do
    putStrLn $ "Downloading " ++ (title episode)
    let urlLink = link episode
        fileNameIndex = last $ elemIndices '/' urlLink
        fileName = drop (fileNameIndex + 1) urlLink
    media <- openUrl urlLink
    B.writeFile fileName media

